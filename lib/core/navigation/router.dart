import 'package:auto_route/auto_route_annotations.dart';
import 'package:what_is_in_the_fridge/core/navigation/root_page.dart';
import 'package:what_is_in_the_fridge/features/fridge/presentation/pages/what_is_in_my_fridge_page.dart';

@autoRouter
class $Router {
  @initial
  RootPage initialPage;
  WhatIsInMyFridgePage whatIsInMyFridgePage;
}