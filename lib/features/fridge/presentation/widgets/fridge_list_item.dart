import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:what_is_in_the_fridge/core/navigation/router.gr.dart';
import 'package:what_is_in_the_fridge/features/fridge/data/entities/fridge_item.dart';
import 'package:what_is_in_the_fridge/features/fridge/presentation/services/fridge_items_service.dart';
import 'package:what_is_in_the_fridge/features/fridge/presentation/widgets/delete_fridge_item_dialog.dart';

class FridgeListItem extends StatelessWidget {
  final FridgeItem fridgeItem;

  FridgeListItem(this.fridgeItem);

  @override
  Widget build(BuildContext context) {
    final fridgeItemStore =
        Injector.getAsReactive<FridgeItemsStore>(context: context);
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Container(
        child: ListTile(
          leading: IconButton(
            icon: Icon(
              Icons.delete,
              color: Colors.redAccent,
            ),
            onPressed: () => 
            Router.navigator.pushNamed(Router.whatIsInMyFridgePage)
            // fridgeItemStore.setState(
            //   (state) => showDialog(
            //       barrierDismissible: false,
            //       context: context,
            //       builder: (BuildContext context) {
            //         return DeleteFridgeItemDialog(
            //           fridgeItem: fridgeItem,
            //         );
            //       }),
            // ),
          ),
          title: Text(
            fridgeItem.name,
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              if (fridgeItem.count > 0)
                IconButton(
                  icon: Icon(Icons.remove, color: Colors.redAccent),
                  onPressed: () => fridgeItemStore
                      .setState((state) => state.decreaseItemCount(fridgeItem)),
                ),
              Text(
                ('${fridgeItem.count}'),
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              IconButton(
                icon: Icon(
                  Icons.add,
                  color: Colors.greenAccent,
                ),
                onPressed: () => fridgeItemStore
                    .setState((state) => state.increaseItemCount(fridgeItem)),
              ),
            ],
          ),
        ),
        decoration: BoxDecoration(
          color: Colors.blueAccent.withOpacity(0.9),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}
