import 'package:what_is_in_the_fridge/features/fridge/data/entities/fridge_item.dart';
import 'package:what_is_in_the_fridge/features/fridge/data/repositories/fridge_repository.dart';

class FridgeItemsStore{
  final FridgeRepository _fridgeRepository;

  FridgeItemsStore(this._fridgeRepository);

  Stream<List<FridgeItem>> startStream(){
    return _fridgeRepository.getFridgeItems();
  }

  void increaseItemCount(FridgeItem fridgeItem){
    _fridgeRepository.updateItem(fridgeItem.incrementCountWithOne());
  }

  void decreaseItemCount(FridgeItem fridgeItem){
      _fridgeRepository.updateItem(fridgeItem.decreaseCountWithOne());
  }

  void addNewFridgeItem(FridgeItem fridgeItem){
    _fridgeRepository.addNewFridgeItem(fridgeItem);
  }
  void deleteFridgeItem(FridgeItem fridgeItem){
    _fridgeRepository.deleteFridgeItem(fridgeItem);
  }
}