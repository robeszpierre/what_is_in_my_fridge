import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:what_is_in_the_fridge/features/fridge/data/entities/fridge_item.dart';
import 'package:what_is_in_the_fridge/features/fridge/data/repositories/fridge_repository.dart';
import 'package:what_is_in_the_fridge/features/fridge/presentation/services/fridge_items_service.dart';
import 'package:what_is_in_the_fridge/features/fridge/presentation/widgets/add_new_fridge_item_dialog.dart';
import 'package:what_is_in_the_fridge/features/fridge/presentation/widgets/fridge_list_item.dart';

class WhatIsInMyFridgePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [
          Inject<FridgeRepository>(() => FridgeRepository()),
          Inject<FridgeItemsStore>(() => FridgeItemsStore(Injector.get())),
          Inject.stream(() => Injector.get<FridgeItemsStore>().startStream()),
        ],
        builder: (context) {
          final streamSnap = Injector.getAsReactive<List<FridgeItem>>(context: context).snapshot;
          return Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 82),
                child: Center(
                  child: Image.asset(
                    "assets/fridge.jpg",
                    height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              Scaffold(
                backgroundColor: Colors.transparent,
                appBar: AppBar(
                  title: Text('Mi van a hűtőmben?'),
                  centerTitle: true,
                ),
                body: streamSnap.hasError
                    ? Text(streamSnap.error.toString())
                    : streamSnap.hasData
                        ? ListView.builder(
                            itemCount: streamSnap.data.length,
                            itemBuilder: (BuildContext context, index) {
                              return FridgeListItem(streamSnap.data.elementAt(index));
                            },
                          )
                        : Center(
                            child: CircularProgressIndicator(),
                          ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8, right: 8),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: FloatingActionButton(
                    backgroundColor: Colors.greenAccent.withOpacity(0.9),
                    child: Icon(Icons.add),
                    onPressed: () {
                      showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (BuildContext context){
                          return AddNewFridgeItemDialog();
                        }
                      );
                    },
                  ),
                ),
              )
            ],
          );
        });
  }
}
