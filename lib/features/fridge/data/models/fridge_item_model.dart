part of 'package:what_is_in_the_fridge/features/fridge/data/entities/fridge_item.dart';

extension FridgeItemModel on FridgeItem {
  FridgeItem incrementCountWithOne(){
    return this.copyWith(count: this.count+1);
  }

  FridgeItem decreaseCountWithOne(){
    if(this.count<=0) return this;
    return this.copyWith(count: this.count-1);
  }
}