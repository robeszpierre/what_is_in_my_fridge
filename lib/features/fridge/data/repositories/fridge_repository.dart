import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:what_is_in_the_fridge/features/fridge/data/entities/fridge_item.dart';

class FridgeRepository {
  Stream<List<FridgeItem>> getFridgeItems() {
    return Firestore.instance
        .collection('user').document('robeszpierre')
        .collection('fridgeItem').snapshots()
        .map((qShot) => qShot.documents
        .map((doc) {
          final docWithDocId = doc.data;
          docWithDocId.addAll({'documentId': doc.documentID});
          return FridgeItem.fromJson(docWithDocId);
        }).toList());
  }

  void updateItem(FridgeItem fridgeItem) {
    Firestore.instance
        .collection('user').document('robeszpierre')
        .collection('fridgeItem').document(fridgeItem.documentId)
        .updateData(fridgeItem.toJson());
  }

  void addNewFridgeItem(FridgeItem fridgeItem) {
    Firestore.instance
        .collection('user').document('robeszpierre')
        .collection('fridgeItem').add(fridgeItem.toJson());
  }

  void deleteFridgeItem(FridgeItem fridgeItem) {
    Firestore.instance
        .collection('user').document('robeszpierre')
        .collection('fridgeItem').document(fridgeItem.documentId).delete();
  }
}
