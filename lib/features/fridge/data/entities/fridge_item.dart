import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'fridge_item.freezed.dart';
part 'fridge_item.g.dart';
part 'package:what_is_in_the_fridge/features/fridge/data/models/fridge_item_model.dart';

@freezed
abstract class FridgeItem with _$FridgeItem {
  factory FridgeItem(String name, int count, [String documentId,]) = _FridgeItem;
  factory FridgeItem.fromJson(Map<String, dynamic> json) => _$FridgeItemFromJson(json);
}