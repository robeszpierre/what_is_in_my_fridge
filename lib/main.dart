import 'package:flutter/material.dart';
import 'package:what_is_in_the_fridge/core/navigation/root_page.dart';
import 'package:what_is_in_the_fridge/core/navigation/router.gr.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mi van a hűtőmben',
      initialRoute: Router.initialPage,
      navigatorKey: Router.navigatorKey,
      onGenerateRoute: Router.onGenerateRoute,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
      ),
      home: RootPage(),
    );
  }
}
